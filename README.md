

# Uber Trip Dataset

This repository contains:

* code for downloading 538's Uber pickups datasets

* code for importing csv into MS SQL Server using R's DBI package

* code for working with database tables using `dbplyr` interface
